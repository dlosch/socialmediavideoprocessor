ffmpeg -i $1.mp4 -vf "scale=720:720:force_original_aspect_ratio=decrease,pad=720:720:(ow-iw)/2:(oh-ih)/2:black" video2.mp4
ffmpeg -i video2.mp4 -c:a copy -c:v libx264 -crf 22 -map 0 -segment_time 30 -g 30 -sc_threshold 0 -force_key_frames "expr:gte(t,n_forced*30)" -f segment whatsapp%03d.mp4              
